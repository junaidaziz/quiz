import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { connect } from 'react-redux'
import { StackNavigator} from 'react-navigation';
import Dashboard from './src/components/dashboard/dashboard';

class App extends Component<{}> {
  constructor (props){
    super(props)
  }

  render() {
    console.disableYellowBox = true;
    Stack = StackNavigator(
      {
        DashboardScreen: {screen: Dashboard}
      },{initialRouteName:'DashboardScreen'}
    );
    return <Stack screenProps={this.props}/>;
  }
}

export default App
