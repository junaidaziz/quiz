# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
A general knowledge quiz.


### Pre-Requisites ###
* Xcode
* Java
* Android Studio OR Android Emulator


### Setup ###
* brew install node

* brew install watchman

* git clone git@github.com:ModernAlkaMe/native-boilerplate.git

* npm install

* npm install -g react-native-cli

* npm install -g react-devtools

* react-native run-ios

For more help visit: https://facebook.github.io/react-native/docs/getting-started.html

### Developer ###
Junaid Aziz
junaidaziz623@gmail.com
