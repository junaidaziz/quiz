export const GET_QUESTIONS_REQUEST = 'GET_QUESTIONS_REQUEST';
export const GET_QUESTIONS_SUCCESS = 'GET_QUESTIONS_SUCCESS';
export const GET_QUESTIONS_ERROR = 'GET_QUESTIONS_ERROR';

export const BooleanOptions = [
  {
    label: 'True',
    value: 'True'
  }, {
    label: 'False',
    value: 'False'
  }];
