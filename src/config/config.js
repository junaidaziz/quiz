const API_BASE_URL = 'https://test.tmh.biomarker.io/';
const GET_USERINFO_URL = 'user/getUserInfo/'

export {
  API_BASE_URL,
  GET_USERINFO_URL
}
