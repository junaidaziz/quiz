import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, ActivityIndicator , TouchableHighlight, ScrollView, Platform, NetInfo, Alert, Button} from 'react-native'
import { connect } from 'react-redux'
import { NavigationActions } from 'react-navigation'
import * as dashboardAction from '../../actions/dashboard'
import _ from 'lodash'
import RadioGroup from 'react-native-custom-radio-group'
import * as constants from '../../constants/dashboard'

var counterInterval

class dashboard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      questionsLoaded: false,
      isStarted: false,
      answersList: [],
      counter: 0,
      correctAnswers: 0
    }

  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state
    return {
      title: 'TheQuiz',
      headerTitleStyle: { fontSize: 20, color:'#50A39D'},
      headerRight: <TouchableHighlight
                     onPress={()=>{  params.onSubmitPress() }}
                     style={{paddingRight: 20}}
                     underlayColor= 'transparent'>
                    <Text style={{color:'#50A39D', fontSize: 18}}>Submit</Text>
                 </TouchableHighlight>
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({ onSubmitPress: this.onSubmitPress })
  }

  componentWillMount() {
    this.props.questionsApi()
  }

  componentWillReceiveProps(nextProps) {
    const{response} = nextProps
    if(response.isQuestionsLoaded) {
      this.setState({questionsLoaded: true});
    }
  }

  onSubmitPress = () => {
    if(this.state.isStarted){
      if(this.state.answersList.length == this.props.response.questions.length){
        clearInterval(counterInterval)
        this.calculateResult()
      }else{
        Alert.alert('Complete Quiz before submission.')
      }
    }else{
      Alert.alert('You must start Quiz before submission.')
    }
  }

  calculateResult = () => {
    let self = this
    let correctAnswersCount = 0
    _.each(this.state.answersList, (answer) => {
      if(answer.correct_answer == answer.selected_answer){
        ++correctAnswersCount
      }
    })

    Alert.alert(
             'Result',
             'You have earned ' +correctAnswersCount+' / '+this.props.response.questions.length+' in '+this.state.counter+ ' Seconds.',
             [
               {text: 'OK', onPress: () => {
                self.setState({isStarted: false, correctAnswers: 0, counter: 0, answersList: []})
               }},
             ],
             { cancelable: false }
           )
  }

  onQuizStarted = () => {
    let self = this
    self.setState({
      isStarted: true
    })
    counterInterval = setInterval(() => {
       self.setState({counter: (self.state.counter+1)})
     }, 1000)

  }

  onQuestionSwitchChange = (value, question) => {
  let answersList = this.state.answersList

  let answeredQuestionIndex = _.findIndex(answersList,  { 'question': question.question})
  if(answeredQuestionIndex == -1){
    question.selected_answer = value
    answersList.push((question))
  }else{
    answersList[answeredQuestionIndex].selected_answer = value
  }

  this.setState({answersList: answersList})
}

  render() {
    const {response} = this.props
    let self = this
    if(!self.state.isStarted){
      return(
        <View style={styles.container}>
            <TouchableHighlight
              style={styles.full_width_btn}
              underlayColor= '#296E69'
              onPress={()=>this.onQuizStarted()}>
                <View style={styles.btn_style}>
                  <Text style={styles.buttonText}>Start Quiz!</Text>
                </View>
            </TouchableHighlight>
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={styles.counterBar}>
          <Text style={styles.counterText}>{self.state.counter} Sec</Text>
        </View>
        <ScrollView>
          {_.map(response.questions, (question, index) => {
                return(
                  <View style={styles.flexCol} key={index.toString()}>

                    <Text style={{color:'#50A39D', fontWeight: 'bold'}}>{question.category}</Text>
                    <Text style={{marginTop: 10}}>{question.question}</Text>

                    <RadioGroup
                      radioGroupList={constants.BooleanOptions}
                      buttonTextStyle={{fontSize:13}}
                      onChange={(val) => this.onQuestionSwitchChange(val, question)}
                      buttonTextActiveStyle={styles.radioButtonTextActiveStyle}
                      buttonTextInactiveStyle={styles.radioButtonTextInactiveStyle}
                      buttonContainerActiveStyle={styles.radioButtonContainerActiveStyle}
                      buttonContainerInactiveStyle={styles.radioButtonContainerInactiveStyle}
                    />
                </View>
                )
          })
        }
        </ScrollView>
      </View>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ccc'
  },
  flexRow: {
    flexDirection: 'row',
    flex: 1,
  },
  flexCol: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    margin: 5,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    alignSelf: 'stretch',
  },
  full_width_btn: {
    backgroundColor: '#50A39D',
    height: 50,
    width:200,
    borderRadius: 6,
  },
  btn_style: {
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 17,
    paddingTop: 13,
    paddingBottom: 16,
    fontWeight: 'bold'
  },
  btn_block: {
    paddingLeft: 20,
    marginBottom: 50,
    paddingRight: 20,
  },
  btn_blockRemoveMargin: {
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: 0,
    marginTop: 10,
  },
radioButtonContainerActiveStyle: {
  width:70,
  height:25,
  borderRadius:10,
  borderColor:'#50A39D',
  borderWidth:2,
  backgroundColor:'#50A39D',
  marginRight:10,
  marginTop:10
},
radioButtonContainerInactiveStyle: {
  width:70,
  height:25,
  borderRadius:10,
  borderStyle:'solid',
  borderColor:'#50A39D',
  borderWidth:2,
  marginRight:10,
  marginTop:10
},
radioButtonTextActiveStyle: {
  color:'#fff',
  fontSize: 12
},
radioButtonTextInactiveStyle: {
  color:'#50A39D',
  fontSize: 12
},
counterBar:{
  padding: 10,
  alignItems: 'center',
  alignSelf: 'stretch',
  justifyContent: 'center',
  backgroundColor:'#50A39D',
},
counterText: {
  color: '#fff',
  fontWeight: 'bold',
  fontSize: 22
}
});

function mapStateToProps(state) {
  return {
    networkStatus: state.app,
    response: state.dashboard,
  }
}

const mapDispatchToProps = dispatch => ({
  questionsApi: () => dispatch(dashboardAction.getQuestions())
})

export default connect(mapStateToProps, mapDispatchToProps)(dashboard)
