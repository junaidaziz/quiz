import { createAction } from 'redux-actions';
import * as constant from '../constants/dashboard';
import * as urlConfig from '../config/config';
import { AsyncStorage } from 'react-native';

export function getQuestions() {
  return function (dispatch, getState) {
    dispatch(questionsRequest())
    let URL = 'https://opentdb.com/api.php?amount=10&category=18&difficulty=easy&type=boolean'

    fetch(URL, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('**** Questions ****>', responseJson);
        if (responseJson.response_code == 0) {
          dispatch(questionsSuccess(responseJson.results))
        }else {
          dispatch(questionsFailure(responseJson))
        }
      })
      .catch((error) => {
        dispatch(questionsFailure(error))
      });
  }
}

export function questionsRequest() {
  return {
    type: constant.GET_QUESTIONS_REQUEST
  }
}

export function questionsSuccess(data) {
  return {
    type: constant.GET_QUESTIONS_SUCCESS,
    data: data
  }
}

export function questionsFailure(data) {
  return {
    type: constant.GET_QUESTIONS_ERROR,
    data: data
  }
}
