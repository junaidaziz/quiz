import * as constant from '../constants/dashboard';

export function dashboard(state ={}, action) {
  switch (action.type) {
    case constant.GET_QUESTIONS_REQUEST:
      return {
          ...state,
          isQuestionsLoaded: false,
          hasError: false
      }
      case constant.GET_QUESTIONS_SUCCESS:
        return {
          ...state,
          isQuestionsLoaded: true,
          hasError: false,
          questions: action.data
      }
      case constant.GET_QUESTIONS_ERROR:
        return {
          ...state,
          isQuestionsLoaded: true,
          hasError: true,
          errorResponse: action.data
      }
    default:
      return state
  }
}
