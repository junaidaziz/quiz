import { Provider } from 'react-redux'
import React, { Component } from 'react'
import {store} from '../store'
import TheQuiz from '../../App'

class Root extends React.Component {
  render() {
    return (
      <Provider store={store}>
       <TheQuiz />
     </Provider>
    )
  }
}
export default Root
